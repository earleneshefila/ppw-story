from django.urls import path
from .views import *

urlpatterns = [
    path('', home, name='home'),
    path('skill', skill, name='skill'),
    path('experience', experience, name='experience'),
    path('education', education, name='education'),
    path('contact', contact, name='contact'),
    path('schedule', schedule, name='schedule'),
    path('story5', schedule, name='story5'),
]