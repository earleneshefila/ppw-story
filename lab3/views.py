from django.shortcuts import render

# Create your views here.
def home(request):
	return render(request, 'home.html')

def skill(request):
	return render(request, 'skill.html')

def experience(request):
	return render(request, 'experience.html')

def education(request):
	return render(request, 'education.html')

def contact(request):
	return render(request, 'contact.html')

def schedule(request):
	return render(request, 'schedule.html')

def schedule_create(request):
	return render(request, 'schedule_create.html')