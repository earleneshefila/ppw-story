from django.db import models
from datetime import datetime

# Create your models here.


class Schedule(models.Model):

    activity = models.CharField(max_length=50)
    date = models.DateTimeField(default=datetime.now)
    time = models.TimeField()
    place = models.CharField(max_length=50)
    category = models.CharField(max_length=50)

    def __str__(self):
        return self.activity
